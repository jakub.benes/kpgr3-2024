#version 330

in vec2 inPosition;

out vec2 texCoord;

void main() {
    // <-1; 1>
    vec2 newPos = inPosition * 2 - 1;
    gl_Position = vec4(newPos, 1., 1.0);

    // Souřadnice do textury
    texCoord = inPosition;
}
