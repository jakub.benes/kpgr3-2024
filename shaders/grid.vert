#version 330

in vec2 inPosition;

uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProj;

out vec2 texCoord;
out vec3 normalVector;
out vec3 lightDirection;

vec3 lightSource = vec3(0, 0, 0.5);

vec3 func(float x, float y){
    return vec3(x, y, 0);
}


vec3 normal(float x, float y){
    vec3 dx = vec3(func(x + 0.01, y) - func(x - 0.01, y));
    vec3 dy = vec3(func(x, y + 0.01) - func(x, y - 0.01));
    return cross(dx, dy);
}

void main() {
    // <-1; 1>
    vec2 position = inPosition * 2 - 1;
    vec3 newPos = func(position.x, position.y);

    //float z = 0.5 * cos(sqrt(20 * pow(newPos.x, 2) + 20 * pow(newPos.y, 2)));
    vec4 posInWorldSpace = uModel * vec4(newPos, 1);
    gl_Position = uProj * uView * posInWorldSpace;

    // Souřadnice do textury
    texCoord = inPosition;

    // Vektor od vrcholu k pozorovateli (view space)
    //viewVector = -posInViewSpace 

    // Normal
    vec3 normal = normal(position.x, position.y);
    normalVector = inverse(transpose(mat3(uModel))) * normal;

    // Vektor ke světlu
    vec4 lightSourceInWorldSpace = vec4(lightSource, 1);
    lightDirection = lightSourceInWorldSpace.xyz - posInWorldSpace.xyz;
}
