#version 330

in vec2 texCoord;
in vec3 normalVector;
in vec3 lightDirection;

uniform sampler2D textureBricks;
uniform sampler2D textureGlobe;

out vec4 outColor;

// Phong
vec4 ambientColor = vec4(0.1, 0.1, 0.1, 1.0);
vec4 diffuseColor = vec4(1.0, 1.0, 1.0, 1.0);

// Spot light
float spotCutOff = 0.7;
vec3 spotDirection = vec3(0, 0, -1.0);


void main() {
    // *** Phong ***
    vec3 ld = normalize(lightDirection); 
    vec3 nd = normalize(normalVector);
    
    // Diffuse
    float nDotL = max(dot(nd, ld), 0.0);
    vec4 totalDiffuse = nDotL * diffuseColor;

    // Final phong
    // outColor = (ambientColor + totalDiffuse) * vec4(1, 1., 1, 1);

    // Spot light
    float spotEffect = max(dot(normalize(spotDirection), normalize(-ld)), 0);
    outColor = vec4(spotEffect, 0, 0, 1.f);
    if(spotEffect > spotCutOff)
        outColor = vec4(1, 1., 1, 1);
    else
        outColor = vec4(0.1, 0.1, 0.1, 1);
    
    // Textura
    //outColor = texture(textureBricks, texCoord);
    
    // Obarvení konstant. barvou
    //outColor = vec4(1, 0, 0, 1);
    
    // Obarvení uv
    //outColor = vec4(texCoord, 0.f, 1.f);
    
    // Obarvení normalou
    //outColor = vec4(nd, 1.f);

    // Obarvení vektor ke světlu
    //outColor = vec4(ld, 1.f);

    // Obarvení spotDirection
    //outColor = vec4(spotDirection, 1.f);
}
