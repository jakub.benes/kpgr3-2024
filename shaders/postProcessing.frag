#version 330

in vec2 texCoord;

uniform sampler2D textureScene;

out vec4 outColor;

void main() {
    if(gl_FragCoord.x > 300)
        outColor = vec4(1,  0, 0, 1) * texture(textureScene, texCoord);
    else
        outColor = texture(textureScene, texCoord);
}
